# pylint: skip-file
import dictionary

def test_dictionary():
    # output = dictionary.some_story()
    assert type(dictionary.some_story) is dict
def test_dict_keys():
    assert dictionary.some_story.keys() == {'start', 'middle', 'end'}
def test_dict_values():
    assert type(dictionary.some_story['start']) is str