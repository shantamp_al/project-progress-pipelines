# pylint: skip-file
# Things I want to test
# 1) Check that their names, age and hair colour are strings
# 2) Check their age is an integer

from strings import *

def test_first_name():
    assert type(FIRST_NAME) == type("")

def test_last_name():
    assert type(LAST_NAME) == type("")

def test_eye_colour():
    assert type(EYE_COLOUR) == type("")

def test_hair_colour():
    assert type(HAIR_COLOUR) == type("")

def test_my_age():
    assert type(AGE) == (int)
    