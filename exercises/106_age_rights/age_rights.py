## Make a program using control flow

# define a function called my_Rights - that takes in your age and returns a string stating your rights

# - 18 - you can vote and drive and drink
# - 17 - You can vote and drive
# - 16 - You can vote
# - > 15 - You're too young, go back to school!

def my_rights (age):
    output = ""
    if(age<16):
        return "You're too young, go back to school!"
    else:
        if(age>15):
            output = output + "You can vote"
        if(age>16):
            output = output + " and drive"
        if(age>17):
            output = output + " and drink"
    return output
